module com.task.calculator {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;

    opens com.task.calculator to javafx.fxml;
    opens com.task.calculator.controller to javafx.fxml;

    exports com.task.calculator.controller;
    exports com.task.calculator;
}