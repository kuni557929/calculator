package com.task.calculator.model;

import java.util.ArrayList;
import java.util.List;

// Просто хранилище для арифм. выражения и ничего больше
public class ArithmeticExpression {

    private static ArithmeticExpression arithmeticExpression;
    private final List<String> expression;

    private ArithmeticExpression() {
        this.expression = new ArrayList<>();
    }

    // Паттерн "Builder"
    public static ArithmeticExpression getInstance() {

        if (arithmeticExpression == null) {
            arithmeticExpression = new ArithmeticExpression();
        }

        return arithmeticExpression;
    }

    public List<String> getExpression() {
        return expression;
    }

    public String getLastElement() {
        return expression.get(getSize() - 1);
    }

    public void replaceLastElement(String element) {
        expression.set(getSize() - 1, element);
    }

    public void addElement(String element) {
        expression.add(element);
    }

    public Integer getSize() {
        return expression.size();
    }

    public void clear() {
        expression.clear();
    }
}
