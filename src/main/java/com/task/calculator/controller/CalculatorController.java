package com.task.calculator.controller;

import com.task.calculator.service.CalculatorService;
import com.task.calculator.service.impl.CalculatorServiceImpl;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.util.EmptyStackException;

public class CalculatorController {

    private static final String DEFAULT_VALUE = "0";
    private static final String EMPTY_VALUE = "";
    private static final String EQUAL_SIGN = "=";
    private static final String REGEX = ".*([а-яА-Яё]|Infinity)+.*"; // выражение, которое проверяет есть ли какое - либо
                                                                    // сообщение об ошибке в главном label

    @FXML
    private Label lblViewer;

    @FXML
    private Label lblInput;

    private final CalculatorService calculatorService;
    private boolean isNumberChosen;

    public CalculatorController() {
        calculatorService = CalculatorServiceImpl.getInstance();
    }

    @FXML
    public void onOperandClicked(MouseEvent event) {

        // Просто достаем операнд из кнопки
        Pane pane = ((Pane) event.getSource());
        String operand = ((Label) pane.getChildren().get(0)).getText();

        // Проверяем по регулярному выражения, если у нас в главном Label есть ошибка
        // К примеру, 'Деление на 0!', то мы очищаем сначала ошибку.
        if (lblInput.getText().matches(REGEX)) {
            clearAll();
        }

        // Если у нас дефолт значение, то есть 0, просто устанавливаем в главный Label наше число
        if (lblInput.getText().equals(DEFAULT_VALUE)) {
            lblInput.setText(operand);
        } else {
            // если это не так, просто конкатенируем наше число к числу из Label
            lblInput.setText(lblInput.getText() + operand);
        }

        // После ставим переменную в true, чтобы показать, что мы выбирали число. Это нужно для дальнейших проверок.
        isNumberChosen = true;
    }

    @FXML
    public void onOperatorClicked(MouseEvent event) {

        // Также достаем оператор из кнопки
        Pane pane = ((Pane) event.getSource());
        String operator = ((Label) pane.getChildren().get(0)).getText();

        // Если в главном Label лежит сообщение об ошибке какое - либо, то мы ничего не делаем.
        if (lblInput.getText().matches(REGEX)) {
            return;
        }

        // Если у нас в верхнем Label если знак '=', это значит, что у нас уже есть какое - либо решение выражения
        // Поэтому надо сначала это решение очистить, а потом проводить дальнейшие вычисления.
        // То есть, мы удаляем старое арифм. выражение
        if (lblViewer.getText().contains(EQUAL_SIGN)) {
            calculatorService.clear();
        }

        // Если мы до момента вызова этого метода выбирали число, то мы добавляем его в арифм. выражение наше
        // Это продемонстрировано в калькуляторе
        // Если нажмешь 2 и как только нажмешь знак, к примеру, '*', то двойка сразу перейдет вместе со знаком вверх
        // За это как раз таки отвечает это условие
        if (isNumberChosen) {
            calculatorService.addOperand(lblInput.getText());
        }

        // Добавляем оператор в арифм. выражение
        calculatorService.addOperator(operator);

        // Меняем текст в калькуляторе
        lblViewer.setText(calculatorService.retrieveArithmeticExpression());
        lblInput.setText(DEFAULT_VALUE);

        // Ну и также ставим переменную в false, т.к. мы только что выбирали не число, а оператор, к примеру, '*'
        isNumberChosen = false;
    }

    @FXML
    public void onBracketClicked(MouseEvent event) {

        // Берем скобку из кнопки
        Pane pane = ((Pane) event.getSource());
        String bracket = ((Label) pane.getChildren().get(0)).getText();

        // Все также, если есть ошибка, то мы ничего не делаем, просто выходим
        if (lblInput.getText().matches(REGEX)) {
            return;
        }

        // Если у нас до этого момента вызова метода было выбрано число какое - либо, то мы добавляем число в арифм. выражение.
        if (isNumberChosen) {
            calculatorService.addOperand(lblInput.getText());
        }

        // Добавляем скобку в наше арифм. выражение
        calculatorService.addOperator(bracket);

        // Меняем текст
        lblViewer.setText(calculatorService.retrieveArithmeticExpression());
        lblInput.setText(DEFAULT_VALUE);

        // Ну и также ставим переменную в false, т.к. выбирали не число,а скобку.
        isNumberChosen = false;
    }

    // Просто очищает все на экране
    @FXML
    public void onClearClicked(MouseEvent event) {
        this.clearAll();
    }

    @FXML
    public void onShiftClicked(MouseEvent event) {

        // Если у нас в главном Label есть сообщение об ошибке, мы очищаем его и выходим
        // Если бы этого условия не было бы, то пользователь мог бы стирать ошибку.
        if (lblInput.getText().matches(REGEX)) {
            clearAll();
            return;
        }

        // Если у нас во втором Label (где у нас показывается само арифм. выражение) есть знак '='
        // Это значит, что у нас есть уже какое - либо решенное арифм. выражение
        // Поэтому, перед тем, как поставить скобку, мы удаляем старое арифм. выражение и очищаем второй Label.
        // А ответ от арифм. выражения сохраняем
        if (lblViewer.getText().contains(EQUAL_SIGN)) {
            calculatorService.clear();
            lblViewer.setText(EMPTY_VALUE);
            return;
        }

        // Если условия сверху не проходят, просто берем число из главного Label
        String inputText = lblInput.getText();

        // Если равен 1 знаку, к примеру, это числа 5, 2, 4, 7, 8, 1, 0 и т.д.
        if (inputText.length() == 1) {
            // Удаляем число и ставим вместо него 0
            lblInput.setText(DEFAULT_VALUE);
            // Ну и также ставим переменную в false, т.к. мы удалили все числа и по сути не выбрали никаких.
            isNumberChosen = false;
        } else {
            // Если не равен 1 знаку, просто убираем последний знак у числа.
            lblInput.setText(inputText.substring(0, inputText.length() - 1));
        }

    }

    @FXML
    public void onSolveClicked(MouseEvent event) {

        // Если также в главном Label есть у нас сообщение об ошибке или во втором Label есть знак '='
        // просто выходим, потому что решать в принципе нечего и если бы не было этого условия, у нас бы появились странные ошибки.
        if (lblInput.getText().matches(REGEX) || lblViewer.getText().contains(EQUAL_SIGN)) {
            return;
        }

        // Ловим исключения, к примеру, деление на 0, или когда стек пустой.
        try {

            // Если у нас арифметическое выражение не оканчивается на ')', значит берем оставшееся число из главного Label
            // и кладем его в наше арифм. выражение.
            if (!lblViewer.getText().matches(".*\\)")) {
                calculatorService.addOperand(lblInput.getText());
            }

            // Берем арифм. выражение, закрываем у него скобки, если таковы имеются.
            String expression = calculatorService.closeOpenParentheses(calculatorService.retrieveArithmeticExpression());

            // Решаем арифм. выражение с помощью метода "польская нотация"
            String answer = String.valueOf(calculatorService.solveArithmeticExpression(expression));

            // показываем ответ
            lblInput.setText(answer);
            lblViewer.setText(expression + "\s" + EQUAL_SIGN);

            // и переменную в true, потому что по сути у нас есть выбранное число - это ответ в главное Label
            isNumberChosen = true;

        } catch (ArithmeticException | IllegalStateException e) {
            lblInput.setText(e.getMessage());
        } catch (EmptyStackException e) {
            lblInput.setText("Ошибка!");
        }
    }

    // просто очищает полностью весь калькулятор
    private void clearAll() {
        calculatorService.clear();
        lblInput.setText(DEFAULT_VALUE);
        lblViewer.setText(EMPTY_VALUE);
        isNumberChosen = false;
    }
}
