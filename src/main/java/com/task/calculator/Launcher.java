package com.task.calculator;

// Лаунчер, который запускает наше приложение, я его добавил, потому что без него были ошибки при создании exe файла
// Этот класс можно удалить.
public class Launcher {
    public static void main(String[] args) {
        CalculatorApplication.main(args);
    }
}
