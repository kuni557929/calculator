package com.task.calculator.service.impl;

import com.task.calculator.service.PolishNotationService;

import java.util.Stack;

public class PolishNotationServiceImpl implements PolishNotationService {

    private static PolishNotationService polishNotationService;

    // Паттерн "Builder"
    public static PolishNotationService getInstance() {

        if (polishNotationService == null) {
            polishNotationService = new PolishNotationServiceImpl();
        }

        return polishNotationService;
    }

    @Override
    public String toRPN(String[] arithmeticExpression) {

        StringBuilder result = new StringBuilder();

        Stack<String> stackOfOperators = new Stack<>();

        // Проходимся по всем элементам выражения
        for (String elementArithmeticExpression : arithmeticExpression) {

            // Берем приоритет каждого элемента (для числа - это 0)
            Integer priorityOfElement = getPriorityOfElement(elementArithmeticExpression);

            // Если равен 0, то есть это число, просто кладем его в результат с пробелом.
            if (priorityOfElement == 0) {

                result.append("\s").append(elementArithmeticExpression);

            // Если равен 1, то это значит, что наш оператор равен '(', просто кладем его в стэк операторов
            } else if (priorityOfElement == 1) {

                stackOfOperators.push(elementArithmeticExpression);

            // Если приоритет 2 или 3, значит перед нами один из знаков (*,/,+,-)
            } else if (priorityOfElement >= 2) {

                // Проходимся по стеку операторов, пока он не будет пустым.
                while (!stackOfOperators.isEmpty()) {

                    // Если приоритет оператора из стека больше или равен нашему приоритету элемента
                    if (getPriorityOfElement(stackOfOperators.peek()) >= priorityOfElement) {

                        // То мы просто добавляем его в результат с пробелом, удаляя из стэка.
                        result.append("\s").append(stackOfOperators.pop());
                    } else {

                        // НО если у нас встречается оператор, приоритет которого меньше, чем наш, то мы выходим из цикла
                        break;
                    }

                    // В общем, если у нас *, а в стеке лежит [+,*,/]
                    // Мы проходимся по всем элементам и смотрит приоритет каждого
                    // приоритет '/' равен приоритету нашего оператора '*', поэтому удаляем из стэка и кладем в результат
                    // [+,*], далее '*', приоритет равен нашему '*', делаем также, удаляем и кладем в результат
                    // [+], приоритет '+' меньше, чем приоритет нашего '*', поэтому мы выходим из цикла и оставляем '+' в стэке
                    // После добавляем '*' в стэк.
                }

                // И далее, кладем наш оператор в стэк.
                stackOfOperators.push(elementArithmeticExpression);

            // Если приоритет равен -1, это значит, что перед нам закрывающая скобка.
            } else if (priorityOfElement == -1) {

                // проходимся по всем элементам, пока не встретится открывающая скобка.
                while (getPriorityOfElement(stackOfOperators.peek()) != 1) {

                    // кладем их в результат с пробелом
                    result.append("\s").append(stackOfOperators.pop());
                }

                // после того как дошли до открывающей скобки, мы ее тоже убираем из стэка.
                stackOfOperators.pop();

            }
        }

        // ну и если у нас после работы алгоритма еще остались элементы в стэке, мы их тоже все в результат кладем.
        while (!stackOfOperators.isEmpty()) {
            result.append("\s").append(stackOfOperators.pop());
        }

        // преобразуем наш результат в строку, убирая пробелы вначале и конце.
        return result.toString().trim();
    }

    @Override
    public Double toAnswer(String polishNotation) {

        Stack<Double> result = new Stack<>();

        String[] polishNotationArray = polishNotation.split("\s");

        for (String element : polishNotationArray) {

            // Если равен нулю, значит элемент - число, поэтому просто добавляем его в result stack.
            if (getPriorityOfElement(element) == 0) {

                result.push(Double.parseDouble(element));

              // Если приоритет 2 или больше, значит перед нами оператор.
            } else if (getPriorityOfElement(element) >= 2) {

                // Берем два числа из stack.
                Double numberFirst = result.pop();
                Double numberSecond = result.pop();

                // Считаем их результат и кладем результат в result stack.
                result.push(calculate(numberFirst, numberSecond, element));
            }

        }

        return result.pop();
    }

    // Приоритет операторов для польской нотации.
    private Integer getPriorityOfElement(String operator) {

        if (operator.matches("[*/]")) {
            return 3;
        }

        if (operator.matches("[+-]")) {
            return 2;
        }

        if (operator.matches("[(]")) {
            return 1;
        }

        if (operator.matches("[)]")) {
            return -1;
        }

        // 0 число - это приоритет любого числа.
        return 0;
    }

    private Double calculate(Double numberFirst, Double numberSecond, String operator) {
        switch (operator) {
            case "+" -> {
                return numberSecond + numberFirst;
            }
            case "-" -> {
                return numberSecond - numberFirst;
            }
            case "*" -> {
                return numberSecond * numberFirst;
            }
            case "/" -> {

                // Если второе число равно нулю, то кидаем исключение.
                if (numberFirst == 0) {
                    throw new ArithmeticException("Деление на 0!");
                }

                return numberSecond / numberFirst;
            }
            default -> throw new IllegalStateException("Ошибка!");
        }
    }
}
