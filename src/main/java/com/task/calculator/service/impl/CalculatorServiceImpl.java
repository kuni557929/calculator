package com.task.calculator.service.impl;

import com.task.calculator.model.ArithmeticExpression;
import com.task.calculator.service.CalculatorService;
import com.task.calculator.service.PolishNotationService;

import java.util.Collections;

/**
 * Реализация {@link CalculatorService} интерфейса
 */
public class CalculatorServiceImpl implements CalculatorService {

    private static CalculatorService calculatorService;

    private final ArithmeticExpression arithmeticExpression;
    private final PolishNotationService polishNotationService;

    private CalculatorServiceImpl() {
        arithmeticExpression = ArithmeticExpression.getInstance();
        polishNotationService = PolishNotationServiceImpl.getInstance();
    }

    // Паттерн "Builder".
    public static CalculatorService getInstance() {

        if (calculatorService == null) {
            calculatorService = new CalculatorServiceImpl();
        }

        return calculatorService;
    }

    @Override
    public Double solveArithmeticExpression(String expression) {

        // Переводит арифметическое выражение в польскую нотацию.
        String polishNotation = polishNotationService.toRPN(expression.split("\s"));

        // Переводит польскую нотация в ответ.
        return polishNotationService.toAnswer(polishNotation);
    }

    @Override
    public String retrieveArithmeticExpression() {
        // Просто берет список, состоящий из операндов и операторов и соединяет их с помощью пробела.
        return String.join("\s", arithmeticExpression.getExpression());
    }

    @Override
    public String closeOpenParentheses(String expression) {

        int count = 0;

        // Считает все скобки и если у нас 'count' после счета не равен нулю, значит есть незакрытые скобки.
        // Пример: если count = 2, то в конец он поставит две закрытые скобки "((2+2" -> "((2+2))" .
        // Приме 2: если count -2, то в начало он поставит две скобки, "2+2))" -> "((2+2))".
        for (int i = 0; i < expression.length() && count >= 0; i++) {
            if (expression.charAt(i) == '(') {
                count++;
            } else if (expression.charAt(i) == ')') {
                count--;
            }
        }

        // Если count < 0, значит у нас не закрытые скобки вначале арифм. выражения.
        if (count < 0) {
            return String.join("", Collections.nCopies(Math.abs(count), "(\s")) + expression;
        }

        // но если count > 0, значит в конце выражения если не закрытые скобки.
        return expression + String.join("", Collections.nCopies(count, "\s)"));
    }

    @Override
    public void addOperand(String operand) {
        // На тот случай, если пользователь будет вводить много нулей, когда у нас в "input" 0 уже стоит.
        if (arithmeticExpression.getSize() == 0 && operand.equals("0")) {
            return;
        }

        // Если все хорошо, то добавляет операнд в наше арифметическое выражение.
        arithmeticExpression.addElement(operand);
    }

    @Override
    public void addOperator(String operator) {

        // Если у нас нет никакого арифметического выражения, т.е. в "lblResult" лежит 0.
        if (arithmeticExpression.getSize() == 0) {

            // Поскольку у нас 0 элементов, то есть нет никакого арифметического выражения,
            // то мы не можем начать наше арифметическое выражение с (/,*,-,+,')'), только с '('.
            if (!operator.matches("[(]")) {
                return;
            }

        } else {

            // Если у нас все же есть какие - либо элементы, то мы берем самый последний
            String lastElement = arithmeticExpression.getLastElement();

            // Если наш последний элемент соответствует одному из операторов (*,/,-,+) и оператор не соответствует "(",
            // то мы просто его заменяем на наш оператор.
            if (!operator.matches("[(]") && lastElement.matches("[*\\-+/]")) {
                arithmeticExpression.replaceLastElement(operator);
                return;
            }

            // Если наш оператор равен '(', а последний элемент - это какое - то число, то мы добавляем еще умножение.
            // Пример: 2 * (...
            // \\d+ - это любые числа
            // В общем, если у нас стоит 2 и мы ставим '(', то он автоматом еще добавит умножение. 2 * (...
            if (operator.matches("[(]") && lastElement.matches("\\d+.*\\d*")) {
                arithmeticExpression.addElement("*");
            }

        }

        // Если ни одно условие не выполнилось, то просто добавляем оператор
        arithmeticExpression.addElement(operator);
    }

    @Override
    public void clear() {

        // Просто очищает арифметическое выражение.
        arithmeticExpression.clear();
    }
}
