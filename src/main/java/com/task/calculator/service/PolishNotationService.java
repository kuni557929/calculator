package com.task.calculator.service;

import java.util.List;

public interface PolishNotationService {

    /**
     * Method designed to translate an arithmetic expression to a polish notation.
     *
     * @param arithmeticExpression - an arithmetic expression.
     * @return a polish notation.
     */
    String toRPN(String[] arithmeticExpression);


    /**
     * Method designed to translate from a polish notation to an answer.
     *
     * @param polishNotation - a polish notation.
     * @return an answer.
     */
    Double toAnswer(String polishNotation);
}
