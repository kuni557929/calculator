package com.task.calculator.service;

import com.task.calculator.model.ArithmeticExpression;

/**
 * Interface for the {@link ArithmeticExpression} entity.
 */
public interface CalculatorService {

    /**
     * Метод, предназначенный для решения арифм. выражения.
     *
     * @param expression - арифм. выражение.
     * @return результат арифм. выражения.
     */
    Double solveArithmeticExpression(String expression);

    /**
     * Метод, предназначенный для получения арифм. выражения.
     *
     * @return арифм. выражение.
     */
    String retrieveArithmeticExpression();

    /**
     * Метод, предназначенный для закрытия незакрытых скобок в выражении.
     *
     * @param expression - выражение
     * @return выражение с закрытыми скобками.
     */
    String closeOpenParentheses(String expression);

    /**
     * Метод, предназначенный для добавления операнда в арифм. выражение
     *
     * @param operand - операнд
     */
    void addOperand(String operand);

    /**
     * Метод, предназначенный для добавления оператора в арифм. выражение.
     *
     * @param operator - оператор
     */
    void addOperator(String operator);

    /**
     * Метод, предназначенный для очистки арифм. выражения.
     */
    void clear();
}
