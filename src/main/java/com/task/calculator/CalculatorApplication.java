package com.task.calculator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class CalculatorApplication extends Application {

    // Просто старт приложения, ничего больше
    // Здесь задается заголовок, размер и т.д.
    // Особо можно не беспокоиться об этом классе.
    @Override
    public void start(Stage stage) throws IOException {
        stage.setScene(new Scene(FXMLLoader.load(Objects.requireNonNull(CalculatorApplication.class.getResource("calculator.fxml")))));
        stage.setTitle("Калькулятор");
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}